# MySchoolManager
MySchoolManager is a control panel for a school.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites
Install node.js 
```bash
sudo apt install nodejs
```
Check if you have the latest version of node 
```bash
nodejs -v
```
Install npm
```bash
sudo apt install npm
```
### Installation

Use the package manager [npm](https://www.npmjs.com/) to install helloworld.

Use node v16 use :
```bash
nvm install 16
```

```bash
npm i
```

## Usage

Start the application dev with :

### Launch project
```bash
npm run start
```

## Built With
* [API AWS](https://aws.amazon.com/fr/free/?trk=36ca612a-b750-4d2d-b0c1-7d0f0863c3d2&sc_channel=ps&sc_campaign=acquisition&sc_medium=ACQ-P%7CPS-GO%7CBrand%7CDesktop%7CSU%7CCore-Main%7CCore%7CFR%7CFR%7CText&ef_id=Cj0KCQjw29CRBhCUARIsAOboZbIA9-iH7g7EkmKkhBliCBSaBGzOJNBAe5_KvJIjsnvpMmFfwXl4yD4aAkx3EALw_wcB:G:s&s_kwcid=AL!4422!3!563933958237!e!!g!!aws&ef_id=Cj0KCQjw29CRBhCUARIsAOboZbIA9-iH7g7EkmKkhBliCBSaBGzOJNBAe5_KvJIjsnvpMmFfwXl4yD4aAkx3EALw_wcB:G:s&s_kwcid=AL!4422!3!563933958237!e!!g!!aws&all-free-tier.sort-by=item.additionalFields.SortRank&all-free-tier.sort-order=asc&awsf.Free%20Tier%20Types=*all&awsf.Free%20Tier%20Categories=*all) - API
* [JavaScript Vanilla](https://fr.wikipedia.org/wiki/JavaScript) - the language

## Versioning
This is the 1.0.0 version

## Authors

* **Akalmie** - *Initial work* - :ocean: [Akalmie](https://gitlab.com/Akalmie)

* **Xen** - *Webpack environement* - :smile_cat: [Xen](https://gitlab.com/xzen769)


