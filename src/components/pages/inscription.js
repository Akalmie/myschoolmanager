export default class AppBar {
  constructor() {
    this.domApp = document.getElementById('app');
    this.render();
  }

  render = () => {
    this.domApp.innerHTML += `
  <div class="signup-form">
    <form action="index.js" method="post">
      <h2>Sign Up</h2>
      <p>Please fill in this form to create an account!</p>
      <hr>
        <div class="form-group">
        <div class="row">
          <div class="col"><input type="text" class="form-control" name="first_name" placeholder="First Name" required="required"></div>
          <div class="col"><input type="text" class="form-control" name="last_name" placeholder="Last Name" required="required"></div>
        </div>
          </div>
          <div class="form-group">
          <div class="row">
          <select name="promo" id="promo-select" class="form-group">
          <option value="">--Choisissez votre promotion--</option>
          <option value="B2">B1</option>
          <option value="B2">B2</option>
          <option value="B3">B3</option>
          <option value="M1">M1</option>
          <option value="M2">M2</option>
      </select>
      <select name="speciality" id="speciality-select" class="form-group">
      <option value="">--Choisissez votre promotion--</option>
      <option value="Developpeur">Developpeur full-stack</option>
      <option value="Marketing">Marketing</option>
      <option value="Webdesign">Webdesign</option>
  </select>
      </div>
          </div>
          <div class="form-group">
            <input type="email" class="form-control" name="email" placeholder="Email" required="required">
          </div>
      <div class="form-group">
              <input type="password" class="form-control" name="password" placeholder="Password" required="required">
          </div>
      <div class="form-group">
              <input type="password" class="form-control" name="confirm_password" placeholder="Confirm Password" required="required">
          </div>        
      <div class="form-group d-grid gap-2 col-6 mx-auto">
      <a href = "https://www.google.com">
            <button type="submit" class="btn btn-primary">Sign Up</button>
          </a>
        </div>
    </form>
  <div class="hint-text text-center">Already have an account? <a href="#">Login here</a></div>
</div>
  `;
  };
}
