/* eslint-disable no-new */
import '../../scss/reset.scss';
import '../../scss/index.scss';
import Navbar from '../navbar';
import Cards from '../cards';

export default class Home {
  constructor() {
    this.domApp = document.getElementById('app');
    this.render();
  }

  render = () => {
    new Navbar();
    new Cards();
  };
}

// eslint-disable-next-line no-new
new Home();
