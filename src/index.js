/* eslint-disable no-new */
import './scss/reset.scss';
import './scss/index.scss';
import Navbar from './components/navbar';
import Cards from './components/cards';

export default class App {
  constructor() {
    this.domApp = document.getElementById('app');
    this.render();
  }

  render = () => {
    new Navbar();
    new Cards();
  };
}

// eslint-disable-next-line no-new
new App();
